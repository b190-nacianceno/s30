db.fruits.insertMany([
     {name: apple,
    color: red,
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: [ "Philippines", "US" ]},

{
    name: banana,
    color: yellow,
    stock: 15, 
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: [ "Philippines", "Ecuador" ]
},

{
    name: kiwi,
    color: green,
    stock: 25 ,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: [ "US", "China" ]
},

{ 
    name:mango,
    color: green,
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: [ "Philippines", "India" ]
}]);


// Section - Using Aggregate Method
/* 
$match 
-used to pass the documents that meet the specified conditions to the next pipeline/aggregation process 
-pipeline/aggregation process is the series of aggregation methods whould the dev/client want to use two or more aggregation methods in one statement. MongoDB will treat this as stages wherein it will not proceed to the next pipeline, unless it is done with the first 
SYNTAX:
    {$match : {field: value}}
*/

db.fruits.aggregate ([
    {$match: {onSale: true}}
]);

/* 
$group - used to group elements together and field-value pairs using the data from the grouped elements
SYNTAX:
    {$group: {_id:"$value",fieldResult: "$valueResult"}}
*/

db.fruits.aggregate([
    {$group: {_id: "$supplier_id",total: {$sum:"$stock"}}}
]);

/* 
using both $match and $group along with aggregation will find for the products that are on sale and will group the total amount of stocks for all suppliers found (this will be done in that specific order)

SYNTAX:
    db.collectionName.aggregate([
        {$}
    ]);
*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id",total: {$sum:"$stock"}}}
]);


/* 
$project - can be used when aggregating data to include/exclude fields from the returned result
SYNTAX:
{$project: {field:1/0}}
*/
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id",total: {$sum:"$stock"}}},
    {$project: {_id: 0}}
]);

/* 
$sort 
-used to change the order of aggreagated results
-providing -1 as the value will result to MongoDB sorting the documents in reverse order

    SYNTAX:
    {$sort : {field: 1/-1}}
*/
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id",total: {$sum:"$stock"}}},
    {$sort: {total: -1}}
]);


/*
code below won't work if the goal is to group the documents based on the number of times a coutnry is mentioned in the "origin" array
db.fruits.aggregate([
    {$group: {_id: "$origin",kinds: {$sum:1}}}
]); */

/* 
$unwind
-used to deconstruct an array field from a collection/field with array value to output a result for each element.
-this will result to creating a separate documents for each fo the element inside the array
SYNTAX:
{$unwind : field}
*/

db.fruits.aggregate([
    {$unwind:"$origin"},
]);


// using the code below, we are instructing Mongo DB to deconstruct first documents based on ht eorigin field and group them based on the number of times a coutnry is mentioned in the "origin" field
db.fruits.aggregate([
    {$unwind:"$origin"},
    {$groud:{_id:"$origin",kinds: {$sum:1}}},

]);

// SECTION - Schema design
/* 
-schema design/data modelling is an important feature when creating databases
-MongoDB documents can be categorized into normalized or denormalized/embedded data 
-normalized  data refers to a data structure where documents are referred to each otehr using their ids for related pieces of information 
-de-normalized data/embedded data design refers to a data structure where related pieces of information are added to a document as an embedded object
*/


var owner = ObjectID();

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact:"09123456789"
});


// mini-activity

// Normalized data schema design in one-to-one realtionship
var suppliers = ObjectId();

db.suppliers.insert({
    name: "Jane Doe",
    contact: "0917345678",
    owner_id:ObjectId("62d5476c210276b56f352eb3")
});

// de-normalized data schema design with one-to-one relationship

db.suppliers.insert({
    name: "DEF fruits",
    contact: "09123456789",
    address: [
        {street:"123 San Jose St.",city: "Manila"},
        {street:"367 Gil Puyat",city: "Makati"}
    ]
});

/* 
-Both data structrures are common practices but each of them has its own pros and cons 

Differences between Normalized and De-normalized

NORMALIZED
-easier to read information becajuse separate documents can be retrieved
-in terms of querying results, it performs slower compared to embedded data due to having to retrieve mutiple documents at the same time
-this useful for data structures where pieces of information are commonly operated on/changed

DE-NORMALIZED
-it makes it easier to query documents and has a faster performance because only one query has to be implemented
-harder to manage when data structure becomes too complex and makes it difficult to manipulate and access information

-This approach is applicable for data structures where pieces of information are commonly retrieved and rarely operated on/changed
*/

// Miniactivity

var supplier = ObjectId();
var branch = ObjectId();



db.suppliers.insert({
    _id: supplier,
    name: "John Cena",
    contact: "09123456789",
    branches: [
branch    ]
});

db.branch.insert({
    _id:  ObjectId("62d55272210276b56f352eba"),
    name: "Evelyn Quan",
    address: "09123456789",
    supplier_id: ObjectId("62d55272210276b56f352eb9")

});
